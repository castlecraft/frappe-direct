import { Injectable, Inject, BadRequestException } from '@nestjs/common';
import { AgendaJobService } from '../../entities/agenda-job/agenda-job.service';
import { AGENDA_TOKEN } from '../../../system-settings/providers/agenda.provider';
import { ObjectId } from 'mongodb';
import { AGENDA_JOB_STATUS } from '../../../constants/app-strings';
import { throwError } from 'rxjs';

@Injectable()
export class JobQueueAggregateService {
  constructor(
    @Inject(AGENDA_TOKEN)
    private readonly jobService: AgendaJobService,
  ) {}

  async list(skip, take, sort, filter, token) {
    return await this.jobService.list(
      Number(skip),
      Number(take),
      sort,
      token,
      filter,
    );
  }

  async retryJob(jobId) {
    return await this.jobService.updateOne(
      { _id: new ObjectId(jobId) },
      {
        $set: {
          nextRunAt: new Date(),
          'data.status': AGENDA_JOB_STATUS.in_queue,
          failCount: 0,
        },
      },
    );
  }

  async getOneDataImportJob(uuid: string) {
    const job = await this.jobService.findOne({ 'data.uuid': uuid });

    if (!job) {
      return throwError(new BadRequestException('Export job dose not exists.'));
    }

    return job;
  }
}
