import { JobQueueAggregateService } from './job-queue-aggregate/job-queue-aggregate.service';
import { SyncAggregateService } from './sync-aggregate/sync-aggregate.service';

export const SyncAggregateManager = [
  JobQueueAggregateService,
  SyncAggregateService,
];
