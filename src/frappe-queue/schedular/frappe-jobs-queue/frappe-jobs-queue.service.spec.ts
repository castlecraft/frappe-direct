import { Test, TestingModule } from '@nestjs/testing';
import { AGENDA_TOKEN } from '../../../system-settings/providers/agenda.provider';
import { FrappeJobService } from './frappe-jobs-queue.service';
import { ConfigService } from '../../../config/config.service';

describe('FrappeJobService', () => {
  let service: FrappeJobService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        FrappeJobService,
        { provide: AGENDA_TOKEN, useValue: {} },
        {
          provide: ConfigService,
          useValue: {},
        },
      ],
    }).compile();

    service = module.get<FrappeJobService>(FrappeJobService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
