import { Injectable, OnModuleInit, Inject, Logger } from '@nestjs/common';
import { AGENDA_TOKEN } from '../../../system-settings/providers/agenda.provider';
import {
  FRAPPE_QUEUE_JOB,
  AGENDA_JOB_STATUS,
  AGENDA_MAX_RETRIES,
} from '../../../constants/app-strings';
import { DateTime } from 'luxon';
import { AgendaJob } from '../../entities/agenda-job/agenda-job.entity';
import {
  AGENDA_JOBS_CONCURRENCY,
  ConfigService,
} from '../../../config/config.service';
import { AGENDA_JOBS_CONCURRENCY_MESSAGE } from '../../../constants/messages';
import * as Agenda from 'agenda';

@Injectable()
export class FrappeJobService implements OnModuleInit {
  constructor(
    @Inject(AGENDA_TOKEN)
    private readonly agenda: Agenda,
    private readonly configService: ConfigService, // here you can provide service which you wish to have callback for any job.
  ) {}
  // eg. private readonly JOB_NAME : JobService

  async onModuleInit() {
    Logger.log(
      AGENDA_JOBS_CONCURRENCY_MESSAGE +
        this.configService.get(AGENDA_JOBS_CONCURRENCY) || '1',
      AGENDA_JOBS_CONCURRENCY,
    );
    this.agenda.define(
      FRAPPE_QUEUE_JOB,
      {
        concurrency:
          Number(this.configService.get(AGENDA_JOBS_CONCURRENCY)) || 1,
      },
      async (job: any, done) => {
        // Please note done callback will work only when concurrency is provided.
        this[job.attrs.data.type]
          .execute(job)
          .toPromise()
          .then(success => {
            job.attrs.data.status = AGENDA_JOB_STATUS.success;
            return done();
          })
          .catch(err => {
            job.attrs.data.status = AGENDA_JOB_STATUS.retrying;
            return done(this.getPureError(err));
          });
      },
    );
    this.agenda.on(`fail:${FRAPPE_QUEUE_JOB}`, (error, job) =>
      this.onJobFailure(error, job),
    );
  }

  resetState(job: AgendaJob) {
    return this[job.data.type].resetState(job);
  }

  async onJobFailure(error: any, job: Agenda.Job<any>) {
    const retryCount = job.attrs.failCount - 1;
    if (retryCount < AGENDA_MAX_RETRIES) {
      job.attrs.data.status = AGENDA_JOB_STATUS.retrying;
      job.attrs.nextRunAt = this.getExponentialBackOff(
        retryCount,
        job.attrs.data.settings.timeZone,
      );
    } else {
      job.attrs.data.status = AGENDA_JOB_STATUS.fail;
    }
    await job.save();
  }

  getExponentialBackOff(retryCount: number, timeZone): Date {
    const waitInSeconds =
      Math.pow(retryCount, 4) + 15 + Math.random() * 30 * (retryCount + 1);
    return new DateTime(timeZone).plus({ seconds: waitInSeconds }).toJSDate();
  }

  replaceErrors(keys, value) {
    if (value instanceof Error) {
      const error = {};

      Object.getOwnPropertyNames(value).forEach(key => {
        error[key] = value[key];
      });

      return error;
    }

    return value;
  }

  getPureError(error) {
    if (error && error.response) {
      error = error.response.data ? error.response.data : error.response;
    }
    try {
      return JSON.parse(JSON.stringify(error, this.replaceErrors));
    } catch {
      return error.data ? error.data : error;
    }
  }
}
