export const FRAPPE_API_GET_USER_INFO_ENDPOINT = '/api/resource/User/';
export const CLIENT_CALLBACK_ENDPOINT = '/home/callback';
export const FRAPPE_API_GET_OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token/';
export const OAUTH_BEARER_TOKEN_ENDPOINT =
  '/api/resource/OAuth%20Bearer%20Token';
export const GET_TIME_ZONE_ENDPOINT = '/api/method/frappe.client.get_time_zone';
export const ERPNEXT_API_WAREHOUSE_ENDPOINT = '/api/resource/Warehouse';
export const FRAPPE_API_GET_USER_PERMISSION_ENDPOINT =
  '/api/resource/User Permission';
export const FRAPPE_API_GET_DOCTYPE_COUNT =
  '/api/method/frappe.client.get_count';
export const API_RESOURCE = '/api/resource/';
