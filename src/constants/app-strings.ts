export const ADMINISTRATOR = 'administrator';
export const SYSTEM_MANAGER = 'System Manager';
export const TOKEN = 'token';
export const AUTHORIZATION = 'authorization';
export const SERVICE = 'frappe-direct';
export const PUBLIC = 'public';
export const APP_NAME = 'frappe-direct';
export const SWAGGER_ROUTE = 'api-docs';
export enum ConnectedServices {
  CommunicationServer = 'communication-server',
  InfrastructureConsole = 'infrastructure-console',
  IdentityProvider = 'identity-provider',
}
export const ACCEPT = 'Accept';
export const BEARER_HEADER_VALUE_PREFIX = 'Bearer ';
export const APPLICATION_JSON_CONTENT_TYPE = 'application/json';
export const CONTENT_TYPE_HEADER_KEY = 'Content-Type';
export const GLOBAL_API_PREFIX = 'api';
export const PASSWORD = 'password';
export const REFRESH_TOKEN = 'refresh_token';
export const OPENID = 'openid';
export const CONTENT_TYPE = 'content-type';
export const APP_WWW_FORM_URLENCODED = 'application/x-www-form-urlencoded';
export const APP_JSON = 'application/json';
export const TEN_MINUTES_IN_SECONDS = 600;
export const REDIRECT_ENDPOINT = '/api/direct/callback';
export const PROFILE_ENDPOINT =
  '/api/method/frappe.integrations.oauth2.openid_profile';
export const AUTH_ENDPOINT = '/api/method/frappe.integrations.oauth2.authorize';
export const REVOKE_ENDPOINT =
  '/api/method/frappe.integrations.oauth2.revoke_token';
export const TOKEN_ENDPOINT =
  '/api/method/frappe.integrations.oauth2.get_token';
export const TWENTY_MINUTES_IN_SECONDS = 20 * 60; // 20 min * 60 sec;
export const SCOPE = 'all openid';
export const ACTIVE = 'Active';
export const REVOKED = 'Revoked';
export const HUNDRED_NUMBERSTRING = '100';
export const MONGO_INSERT_MANY_BATCH_NUMBER = 10000;
export const VALIDATE_AUTH_STRING = 'validate_oauth';
export const TOKEN_HEADER_VALUE_PREFIX = 'token ';
export const FRAPPE_QUEUE_JOB = 'FRAPPE_QUEUE_JOB';
// following fields would be listed in API for job_queue/v1/list.
export const FRAPPE_JOB_SELECT_FIELDS = [
  'name',
  'failedAt',
  'failCount',
  'failReason',
  'data.status',
  'data.parent',
  'data.payload',
  'data.token.fullName',
  'data.sales_invoice_name',
  'data.uuid',
  'data.type',
];
export const AGENDA_JOB_STATUS = {
  success: 'Successful',
  fail: 'Failed',
  in_queue: 'In Queue',
  reset: 'Reset',
  retrying: 'Retrying',
  exported: 'Exported',
};
export const AGENDA_MAX_RETRIES = 1;
export const AGENDA_DATA_IMPORT_MAX_RETRIES = 2;
export const DATA_IMPORT_DELAY = 1339;
export const ALL_TERRITORIES = 'All Territories';
export const MULTIPART_FORM_DATA = 'multipart/form-data';
export const INVALID_REGEX = ['+', '(', ')'];
export function PARSE_REGEX(value: string) {
  return value
    .split('')
    .map(char => {
      return INVALID_REGEX.includes(char) ? `\\${char}` : char;
    })
    .join('');
}
export const API_COMMAND_USER = '/api/command/user';
export const APP_URL_PROFILE_ENDPOINT = '/api/connected_device/openid_profile';
export const APP_URL_REVOKE_ENDPOINT = '/api/connected_device/revoke_token';
export const APP_URL_GET_TOKEN_ENDPOINT = '/api/connected_device/get_token';
