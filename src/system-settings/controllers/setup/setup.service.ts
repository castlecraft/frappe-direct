import { Injectable, HttpService } from '@nestjs/common';
import { PLEASE_RUN_SETUP } from '../../../constants/messages';
import {
  APP_URL_PROFILE_ENDPOINT,
  APP_URL_REVOKE_ENDPOINT,
  APP_URL_GET_TOKEN_ENDPOINT,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';

@Injectable()
export class SetupService {
  constructor(
    protected readonly settings: ServerSettingsService,
    protected readonly http: HttpService,
  ) {}

  async setup(params) {
    return await this.settings.save(params);
  }

  async getInfo() {
    const info = await this.settings.findOne();

    if (!info) {
      return { message: PLEASE_RUN_SETUP };
    }

    info.webhookApiKey = undefined;
    info._id = undefined;
    info.serviceAccountApiSecret = undefined;
    info.serviceAccountSecret = undefined;
    info.serviceAccountApiKey = undefined;

    return {
      ...info,
      appURL: info.appURL,
      authorizationURL: info.authorizationURL,
      authServerURL: info.authServerURL,
      brand: info.brand,
      callbackProtocol: info.callbackProtocol,
      clientId: info.backendClientId,
      defaultCompany: info.defaultCompany,
      profileURL: info.appURL + APP_URL_PROFILE_ENDPOINT,
      revocationURL: info.appURL + APP_URL_REVOKE_ENDPOINT,
      service: info.service,
      scope: info.scope,
      timeZone: info.timeZone,
      tokenURL: info.appURL + APP_URL_GET_TOKEN_ENDPOINT,
      uuid: info.uuid,
    };
  }
}
