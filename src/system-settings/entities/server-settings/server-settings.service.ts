import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MongoRepository } from 'typeorm';
import { ServerSettings } from './server-settings.entity';
import { SettingsAlreadyExistsException } from '../../../constants/exceptions';
import { DEFAULT } from '../../../constants/typeorm.connection';
import { randomBytes } from 'crypto';
import {
  PROFILE_ENDPOINT,
  AUTH_ENDPOINT,
  TOKEN_ENDPOINT,
  REVOKE_ENDPOINT,
  SCOPE,
} from '../../../constants/app-strings';

@Injectable()
export class ServerSettingsService {
  constructor(
    @InjectRepository(ServerSettings, DEFAULT)
    private readonly repo: MongoRepository<ServerSettings>,
  ) {}

  async save(params) {
    if (await this.repo.count()) {
      throw new SettingsAlreadyExistsException();
    }

    const settings = new ServerSettings();
    Object.assign(settings, params);
    settings.profileURL = settings.authServerURL + PROFILE_ENDPOINT;
    settings.authorizationURL = settings.authServerURL + AUTH_ENDPOINT;
    settings.tokenURL = settings.authServerURL + TOKEN_ENDPOINT;
    settings.revocationURL = settings.authServerURL + REVOKE_ENDPOINT;
    settings.scope = SCOPE.split(' ');
    // settings.frontendCallbackURLs = [settings.appURL];
    // settings.backendCallbackURLs = [settings.appURL];
    settings.webhookApiKey = randomBytes(64).toString('hex');
    return await this.repo.save(settings);
  }

  async findOne(params?): Promise<ServerSettings> {
    return await this.repo.findOne(params);
  }

  async find(params?) {
    return await this.repo.find(params);
  }

  async update(query, params) {
    return await this.repo.update(query, params);
  }

  async updateOne(query, params) {
    return await this.repo.updateOne(query, params);
  }

  async count() {
    return this.repo.count();
  }
}
