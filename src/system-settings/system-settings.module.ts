import { Module, Global, HttpModule } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { SettingsController } from './controllers/settings/settings.controller';
import { SetupController } from './controllers/setup/setup.controller';
import { SetupService } from './controllers/setup/setup.service';
import { SystemSettingsAggregates } from './aggregates';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ServerSettings } from './entities/server-settings/server-settings.entity';
import { DEFAULT } from '../constants/typeorm.connection';
import { ServerSettingsService } from './entities/server-settings/server-settings.service';
import { AgendaProvider } from './providers/agenda.provider';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([ServerSettings], DEFAULT),
    HttpModule,
    CqrsModule,
  ],
  providers: [
    SetupService,
    AgendaProvider,
    ServerSettingsService,
    ...SystemSettingsAggregates,
  ],
  controllers: [SettingsController, SetupController],
  exports: [
    SetupService,
    AgendaProvider,
    ServerSettingsService,
    ...SystemSettingsAggregates,
  ],
})
export class SystemSettingsModule {}
