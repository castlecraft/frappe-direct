import { Injectable, BadRequestException, HttpService } from '@nestjs/common';
import { INVALID_CODE, INVALID_STATE } from '../../../constants/messages';
import {
  APP_WWW_FORM_URLENCODED,
  AUTHORIZATION,
  CONTENT_TYPE,
  SERVICE,
} from '../../../constants/app-strings';
import { ServerSettingsService } from '../../../system-settings/entities/server-settings/server-settings.service';
import { OAuth2GetTokenDto } from '../../controllers/connected-device/oauth2-get-token.dto';
import { from } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { stringify } from 'querystring';

@Injectable()
export class ConnectedDeviceService {
  constructor(
    private readonly settings: ServerSettingsService,
    private readonly http: HttpService,
  ) {}

  async relayCodeAndState(code: string, state: string, res) {
    const settings = await this.settings.findOne();
    const callbackProtocol = (settings && settings.callbackProtocol) || SERVICE;
    const url = `${callbackProtocol}://callback?code=${code}&state=${state}`;

    if (!code) {
      throw new BadRequestException(INVALID_CODE);
    }
    if (!state) {
      throw new BadRequestException(INVALID_STATE);
    }

    return res.redirect(url);
  }

  relayGetToken(payload: OAuth2GetTokenDto) {
    return from(this.settings.findOne()).pipe(
      switchMap(settings => {
        return this.http.post(settings.tokenURL, stringify(payload as any), {
          headers: { [CONTENT_TYPE]: APP_WWW_FORM_URLENCODED },
        });
      }),
      map(res => res.data),
    );
  }

  relayGetProfile(accessTokenHeader: string) {
    return from(this.settings.findOne()).pipe(
      switchMap(settings => {
        return this.http.get(settings.profileURL, {
          headers: { [AUTHORIZATION]: accessTokenHeader },
        });
      }),
      map(res => res.data),
    );
  }

  relayRevokeToken(token: string) {
    if (!token || token === 'undefined') {
      return {};
    }

    return from(this.settings.findOne()).pipe(
      switchMap(settings => {
        return this.http.post(settings.revocationURL, stringify({ token }), {
          headers: { [CONTENT_TYPE]: APP_WWW_FORM_URLENCODED },
        });
      }),
      map(res => res.data),
    );
  }
}
