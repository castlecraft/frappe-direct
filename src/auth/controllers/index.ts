import { ConnectedDeviceController } from './connected-device/connected-device.controller';
import { ConnectController } from './connect/connect.controller';
import { CommandController } from './command/command.controller';

export const AuthControllers = [
  ConnectedDeviceController,
  ConnectController,
  CommandController,
];
