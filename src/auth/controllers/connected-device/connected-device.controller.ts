import {
  Controller,
  Query,
  Get,
  Res,
  Post,
  Body,
  UsePipes,
  ValidationPipe,
  Headers,
} from '@nestjs/common';
import { AUTHORIZATION } from '../../../constants/app-strings';
import { ConnectedDeviceService } from '../../aggregates/connected-device/connected-device.service';
import { OAuth2GetTokenDto } from './oauth2-get-token.dto';

@Controller('connected_device')
export class ConnectedDeviceController {
  constructor(private readonly service: ConnectedDeviceService) {}

  @Get('callback')
  async relayCodeAndState(
    @Query('code') code: string,
    @Query('state') state: string,
    @Res() res,
  ) {
    return await this.service.relayCodeAndState(code, state, res);
  }

  @Post('get_token')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  getToken(@Body() payload: OAuth2GetTokenDto) {
    return this.service.relayGetToken(payload);
  }

  @Get('openid_profile')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  getProfile(@Headers(AUTHORIZATION) authorizationHeader: string) {
    return this.service.relayGetProfile(authorizationHeader);
  }

  @Post('revoke_token')
  @UsePipes(new ValidationPipe({ forbidNonWhitelisted: true }))
  revokeToken(@Body('token') token: string) {
    return this.service.relayRevokeToken(token);
  }
}
