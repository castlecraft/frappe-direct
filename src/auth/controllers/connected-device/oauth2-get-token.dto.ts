import { IsEnum, IsOptional, IsString, IsUrl } from 'class-validator';

export enum GrantType {
  authorization_code = 'authorization_code',
  refresh_token = 'refresh_token',
}

export class OAuth2GetTokenDto {
  @IsEnum(GrantType)
  grant_type: string;

  @IsUrl()
  redirect_uri: string;

  @IsString()
  client_id: string;

  @IsString()
  scope: string;

  @IsOptional()
  @IsString()
  code: string;

  @IsOptional()
  @IsString()
  refresh_token: string;
}
