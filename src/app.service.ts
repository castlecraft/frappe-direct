import { Injectable } from '@nestjs/common';
import { SERVICE } from './constants/app-strings';
import { SetupService } from './system-settings/controllers/setup/setup.service';
import { PLEASE_RUN_SETUP } from './constants/messages';

@Injectable()
export class AppService {
  constructor(private readonly setup: SetupService) {}

  async info() {
    let info;
    try {
      info = await this.setup.getInfo();
    } catch (error) {
      info = {
        message: PLEASE_RUN_SETUP,
      };
    }
    return info;
  }

  getRoot() {
    return { service: SERVICE };
  }
}
