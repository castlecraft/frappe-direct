# NestJS Based Resource Server

This resource server is connected to Frappe/ERPNext's built-in Authorization Server

POST /api/setup to setup service
GET /api/info to get service info

Use to start app development for NestJS backend connected to Frappe/ERPNext

### Prerequisites

- MongoDB (use latest official)
- NodeJS (use nvm)

To quick start mongodb with `token-cache` and `frappe-direct` dbs use docker-compose.

```sh
# Copy example env
cp env-example .env
# Start services
docker-compose --project-name frappedirect -f docker/docker-compose.yml up -d
# Start node app
npm run start:debug
```

Note: Make sure your user is part of `docker` group.

### Guards

- Use `TokenGuard` to get cached token in `req.token`
- Use `@Roles(...roles: string[])` decorator to only allow given frappe roles.
- Use `FrappeWebhookGuard` to guard hooked endpoints. Frappe Webhook header must pass `x-frappe-api-key`

### ConnectController

- Use `/connect/v1/token_added` endpoint to listen to added tokens from Authorization Server
- Use `/connect/v1/token_delete` endpoint to listen to deleted tokens from Authorization Server
- Both endpoints are guarded by `FrappeWebhookGuard`
- Add 2 webhooks on frappe

### Health checks

- available on GET /api/healthz
- ping MongoDB Connection

### Setup on Frappe/ERPNext side

1. Setup `Social Login Key` for frappe, DO NOT enable it, Use your site url as `base_url`

Example:

```json
{
    "name": "frappe",
    "enable_social_login": 0,
    "social_login_provider": "Frappe",
    "provider_name": "Frappe",
    "icon": "/assets/frappe/images/favicon.png",
    "base_url": "http://default.localhost:8000",
    "authorize_url": "/api/method/frappe.integrations.oauth2.authorize",
    "access_token_url": "/api/method/frappe.integrations.oauth2.get_token",
    "redirect_url": "/api/method/frappe.www.login.login_via_frappe",
    "api_endpoint": "/api/method/frappe.integrations.oauth2.openid_profile",
    "custom_base_url": 1,
    "auth_url_data": "{\"response_type\": \"code\", \"scope\": \"openid\"}",
    "doctype": "Social Login Key"
}
```

2. Add Backend and frontend Clients

Examples:

Frontend client (Implicit Grant), Note the `client_id` created as `frontendClientId`

```json
{
    "app_name": "Frappe Direct Frontend",
    "skip_authorization": 1,
    "scopes": "all openid",
    "redirect_uris": "http://direct.localhost:4700/callback http://direct.localhost:4700/silent-refresh.html",
    "default_redirect_uri": "http://direct.localhost:4700/callback",
    "grant_type": "Implicit",
    "response_type": "Token",
    "doctype": "OAuth Client"
}
```

Backend Client (Authorization Code Grant and Password Grant), Note the `client_id` created as `backendClientId`

```json
{
    "app_name": "Frappe Direct Backend",
    "skip_authorization": 1,
    "scopes": "all openid",
    "redirect_uris": "http://direct.localhost:4700/api/direct/callback",
    "default_redirect_uri": "http://direct.localhost:4700/api/direct/callback",
    "grant_type": "Authorization Code",
    "response_type": "Code",
    "doctype": "OAuth Client"
}
```

3. Add Service Account

Add a System Manager Account with all roles to be used by services to access ERPNext System.

Note the `username` as `serviceAccountUser` and `password` as `serviceAccountSecret`


### Continue setup on frappe-direct based resource server

1. Setup and note `webhookApiKey` from response

POST /api/setup

```json
{
    "appURL": "http://direct.localhost:4700",
    "authServerURL": "http://default.localhost:8000",
    "frontendClientId": "0191176784",
    "backendClientId": "32901fda83",
    "serviceAccountUser": "bot@castlecraft.in",
    "serviceAccountSecret": "Secret@CBD420",
}
```

4. Add Webhooks

Add Webhooks for `token_delete` and `token_added` endpoints. Use noted `webhookApiKey` for the `x-frappe-api-key` header.

```json
{
    "name": "OAuth Bearer Token-on_trash",
    "webhook_doctype": "OAuth Bearer Token",
    "webhook_docevent": "on_trash",
    "request_url": "http://direct.localhost:8000/api/connect/v1/token_delete",
    "request_structure": "Form URL-Encoded",
    "doctype": "Webhook",
    "webhook_headers": [
        {
            "key": "Content-Type",
            "value": "application/json"
        },
        {
            "key": "x-frappe-api-key",
            "value": "8016dad2fdaf8cd1059aa9...f1ee6"
        }
    ],
    "webhook_data": [
        {
            "fieldname": "client",
            "key": "client"
        },
        {
            "fieldname": "user",
            "key": "user"
        },
        {
            "fieldname": "scopes",
            "key": "scopes"
        },
        {
            "fieldname": "access_token",
            "key": "access_token"
        },
        {
            "fieldname": "name",
            "key": "name"
        },
        {
            "fieldname": "status",
            "key": "status"
        },
        {
            "fieldname": "expires_in",
            "key": "expires_in"
        },
        {
            "fieldname": "refresh_token",
            "key": "refresh_token"
        }
    ]
}
```

```json
{
    "name": "OAuth Bearer Token-after_insert",
    "webhook_doctype": "OAuth Bearer Token",
    "webhook_docevent": "after_insert",
    "request_url": "http://direct.localhost:8000/api/connect/v1/token_added",
    "request_structure": "Form URL-Encoded",
    "doctype": "Webhook",
    "webhook_headers": [
        {
            "key": "Content-Type",
            "value": "application/json"
        },
        {
            "key": "x-frappe-api-key",
            "value": "8016dad2fdaf8cd1059aa9...f1ee6"
        }
    ],
    "webhook_data": [
        {
            "fieldname": "client",
            "key": "client"
        },
        {
            "fieldname": "user",
            "key": "user"
        },
        {
            "fieldname": "scopes",
            "key": "scopes"
        },
        {
            "fieldname": "access_token",
            "key": "access_token"
        },
        {
            "fieldname": "name",
            "key": "name"
        },
        {
            "fieldname": "status",
            "key": "status"
        },
        {
            "fieldname": "expires_in",
            "key": "expires_in"
        },
        {
            "fieldname": "refresh_token",
            "key": "refresh_token"
        }
    ]
}
```
